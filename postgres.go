package main

import (
	"context"
	retry "github.com/avast/retry-go"
	"github.com/jackc/pgx"
)

type DB struct {
	context context.Context
	conn    *pgx.Conn
}

func initDB(svc string) (DB, error) {
	db := DB{context.Background(), nil}
	err := retry.Do(
		func() error {
			var err error
			db.conn, err = pgx.Connect(db.context, svc)
			if err != nil {
				return err
			}
			return nil
		},
		retry.Attempts(5),
		retry.DelayType(retry.BackOffDelay),
		retry.LastErrorOnly(true),
		retry.RetryIf(func(err error) bool {
			return err != nil
		}),
	)

	return db, err
}

func (db DB) close() error {
	return db.conn.Close(db.context)
}

func (db DB) saveCrawlFinish2DB(vendor, when string) error {
	_, err := db.conn.Exec(db.context,
		"insert into crawls (vendor, finish) values ($1, $2)",
		vendor, when)
	return err
}
