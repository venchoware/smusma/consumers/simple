package main

import (
	"fmt"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	log "gitlab.com/venchoware/go/log4go"
	"sync"
)

// retry "github.com/avast/retry-go"
// "time"

type message map[string]interface{}

type queueProcessor struct {
	Qname   string
	Handler func(message, cache, DB) error
}

var queues = []queueProcessor{
	{"crawl_queue", crawlEvents},
}

func main() {
	pflag.String("mq_service", "", "Message queue service")
	pflag.String("loglevel", "INFO", "Log level")
	pflag.String("cache_service", "", "Cache service")
	pflag.Int("cache_max", 5, "Max # of cache threads")
	pflag.String("db_service", "", "DB service")
	pflag.String("db_name", "smusmadb", "DB name")
	pflag.Parse()

	initEnvVars("smconsume")
	viper.BindPFlags(pflag.CommandLine)

	lls := viper.GetString("loglevel")
	ll, err := log.Str2Level(lls)
	if err != nil {
		log.Fatal("Unknown log level: (%s)", lls)
	}
	log.NewDefaultLogger(ll)

	mq_service := viper.GetString("mq_service")
	if mq_service == "" {
		log.Fatal("mq_service must be set!")
	}
	cache_service := viper.GetString("cache_service")
	if cache_service == "" {
		log.Fatal("cache_service must be set!")
	}
	db_service := viper.GetString("db_service")
	if db_service == "" {
		log.Fatal("db_service must be set!")
	}

	cache, err := initCache(cache_service, viper.GetInt("cache_max"))
	if err != nil {
		log.Fatal("Cannot connect to cache: %v", err)
	}
	defer cache.close()

	db, err := initDB(db_service)
	if err != nil {
		log.Fatal("Cannot connect to db: %v", err)
	}
	defer db.close()

	var mq messageQueue
	mq, err = initMQ(mq_service)
	if err != nil {
		log.Fatal("Cannot connect to message queue (%s): %v",
			mq_service, err)
	}
	defer mq.close()

	mq.consume(queues, cache, db)
}

func initEnvVars(prefix string) {
	viper.SetEnvPrefix(prefix)
	viper.BindEnv("mq_service")
	viper.BindEnv("cache_service")
	viper.BindEnv("cache_max")
	viper.BindEnv("db_service")
	viper.BindEnv("db_name")
	viper.BindEnv("loglevel")
}

func (msgq messageQueue) consume(queues []queueProcessor, c cache, db DB) {
	var wg sync.WaitGroup
	for _, queue := range queues {
		wg.Add(1)
		go func() {
			defer wg.Done()
			msgq.consume_from(queue, c, db)
		}()
	}
	wg.Wait()
}

func crawlEvents(data message, c cache, d DB) error {
	vendor := data["Vendor"].(string)
	log.Trace("Decoded message vendor: %s", vendor)

	when := data["When"].(string)
	log.Trace("Decoded message when: %s", when)

	switch data["Type"] {
	case "start":
		log.Info("Start message")
		// saveCrawlStart2DB(vendor, when)
	case "finish":
		log.Info("Finish message")
		if err := d.saveCrawlFinish2DB(vendor, when); err != nil {
			log.Error("Problem saving to db (%s): %v",
				vendor, err)
		}
		if err := c.saveCrawlFinish2Cache(vendor, when); err != nil {
			log.Error("Problem saving to cache (%s): %v",
				vendor, err)
		}
	case "unnecessary":
		log.Info("Unnecessary message")
	default:
		return fmt.Errorf("unknown message type: %s", data["Type"])
	}

	return nil
}
