package main

import (
	"encoding/json"
	"github.com/spf13/viper"
	"github.com/streadway/amqp"
	log "gitlab.com/venchoware/go/log4go"
)

// retry "github.com/avast/retry-go"
// "time"

type messageQueue struct {
	conn *amqp.Connection
	ch   *amqp.Channel
	q    amqp.Queue
}

func initMQ(service string) (messageQueue, error) {
	var msgq messageQueue

	conn, err := amqp.Dial(service)
	if err != nil {
		return msgq, err
	}
	msgq.conn = conn

	ch, err := conn.Channel()
	if err != nil {
		return msgq, err
	}
	msgq.ch = ch

	q, err := ch.QueueDeclare(
		viper.GetString("qname"), // name
		false,                    // durable
		false,                    // delete when unused
		false,                    // exclusive
		false,                    // no-wait
		nil,                      // arguments
	)
	if err != nil {
		return msgq, err
	}
	msgq.q = q

	return msgq, nil
}

func (msgq messageQueue) close() {
	msgq.conn.Close()
	msgq.ch.Close()
}

func (msgq messageQueue) consume_from(q queueProcessor, c cache, db DB) error {
	msgs, err := msgq.ch.Consume(
		q.Qname, // queue
		"",      // consumer
		false,   // auto-ack
		false,   // exclusive
		false,   // no-local
		false,   // no-wait
		nil,     // args
	)
	if err != nil {
		log.Fatal("Problem setting up consumer for [%s]: %v",
			q.Qname, err)
		return err
	}
	log.Info("Consuming messages from [%s]", q.Qname)

	for msg := range msgs {
		var data message
		json.Unmarshal(msg.Body, &data)

		err = q.Handler(data, c, db)
		if err != nil {
			log.Error("Problem handling event: %v", err)
		}
		msg.Ack(false)
	}

	return nil
}
