package main

import (
	retry "github.com/avast/retry-go"
	"github.com/mediocregopher/radix"
)

type cache struct {
	Client *radix.Pool
}

func initCache(service string, max int) (cache, error) {
	var c cache
	err := retry.Do(
		func() error {
			var err error
			c.Client, err = radix.NewPool("tcp", service, max)
			if err != nil {
				return err
			}
			return nil
		},
		retry.Attempts(5),
		retry.DelayType(retry.BackOffDelay),
		retry.LastErrorOnly(true),
		retry.RetryIf(func(err error) bool {
			return err != nil
		}),
	)

	return c, err
}

func (c cache) saveCrawlFinish2Cache(vendor, when string) error {
	return c.Client.Do(radix.Cmd(nil, "SET", vendor, when))
}

func (c cache) close() error {
	return c.Client.Close()
}
